---
title: "Pré-requis"

description: "Ou comment gagner du temps sur l'aventure"
cascade:
  featured_image: "images/aidedd.org.jpg"
---
Pour cette aventure, vous aurez besoin de créer votre personnage. Vous pouvez faire cela en avance et gagner du temps pour la suite.

Ce personnage sera constitué de plusieurs éléments :
 
1. un compte [GitLab](comptes/gitlab/)
2. un compte [GitPod](comptes/gitpod/) associé au compte GitLab

Si vous avez déjà tout ou partie de ces éléments, vous pouvez les réutiliser.

(Un dernier élément : le compte CIVO sera créé pendant la séance)
